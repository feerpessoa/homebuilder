
<style type="text/css">

    .parceiros img{
        width:120px;


    }
</style>


<div id="right">
    <h2>Parceiros</h2>
    <div id="welcome">
        <div class="parceiros">
            <ul>


                <?php
                foreach ($parceiro as $parceiros): ?>

                    <?php if(isset($parceiros['Partner']['dir'])):

                        $parceiros['Partner']['dir'] = explode("\\",$parceiros['Partner']['dir']);
                        $parceiros['Partner']['dir'] = implode('/',$parceiros['Partner']['dir']);

                        $img = $parceiros['Partner']['dir'].'/thumb/pequena/'.$parceiros['Partner']['filename']?>

                        <li><a href="<?php echo $parceiros['Partner']['link']; ?>"><?php echo $this->Html->image('/painel/'.$img) ?></a></li>

                    <?php endif; ?>
                <?php endforeach; ?>

            </ul>

            <div class="controles">
                <?=$this->Paginator->prev('<< Anterior', array(), NULL, array('class' => 'disabled'));?>

                <?=$this->Paginator->numbers();?>

                <?=$this->Paginator->next('Próximo >>', array(), NULL, array('class' => 'disabled'));?>

            </div>
        </div>
    </div>
</div>