
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
						<li class="list-group-item"><?php echo $this->Html->link(__('Editar Produto'), array('action' => 'edit', $novidade['Produto']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Apagar Produto'), array('action' => 'delete', $novidade['Produto']['id']), array('class' => ''), __('Tem certeza que deseja apagar este item? %s', $novidade['Produto']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Visualizar Produtos'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Novo Produto'), array('action' => 'add'), array('class' => '')); ?> </li>

			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="novidades view">

			<h2><?php  echo __('Produto'); ?></h2>

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
     <tr>
		<td><strong><?php echo __('Titulo'); ?></strong></td>
		<td>
			<?php echo h($novidade['Produto']['titulo']); ?>
			&nbsp;
		</td>
     </tr>
<tr>		<td><strong><?php echo __('Pagina do produto'); ?></strong></td>
		<td>
			<?php echo $novidade['Produto']['link']; ?>
			&nbsp;
		</td>
</tr>
     <tr>		<td><strong><?php echo __('Preço'); ?></strong></td>
            <td><?php echo (!empty($novidade['Produto']['preco']))?  'R$' .number_format($novidade['Produto']['preco'], 2, ',', '.') : "Não Informado"; ?>&nbsp;</td>
     </tr>

     <tr>		<td><strong><?php echo __('Preço de leilão'); ?></strong></td>
         <td><?php echo (!empty($novidade['Produto']['preco_leilao']))?  'R$' .number_format($novidade['Produto']['preco_leilao'], 2, ',', '.') : "Não Informado"; ?>&nbsp;</td>
     </tr>

        <tr>		<td><strong><?php echo __('Criação'); ?></strong></td>
		<td>
			<?php echo h($novidade['Produto']['created']); ?>
			&nbsp;
		</td>
     </tr>
        <tr>		<td><strong><?php echo __('Imagem Principal'); ?></strong></td>
            <td>
                <?php if(isset($novidade['Produto']['dir'])):

                    $novidade['Produto']['dir'] = explode("\\",$novidade['Produto']['dir']);
                    $novidade['Produto']['dir'] = implode('/',$novidade['Produto']['dir']);

                    $img = $novidade['Produto']['dir'].'/'.$novidade['Produto']['filename']?>

                    <?php echo $this->Html->image('/'.$img) ?>
                <?php endif; ?>
            </td>
        </tr>

        <tr>		<td><strong><?php echo __('Ultima Atualização'); ?></strong></td>
		<td>
			<?php echo h($novidade['Produto']['modified']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->



</div><!-- /#page-container .row-fluid -->
