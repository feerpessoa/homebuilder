<?php
App::uses('AppController', 'Controller');
/**
 * Novidades Controller
 *
 * @property Novidade $Novidade
 * @property PaginatorComponent $Paginator
 */
class HomeController extends AppController {


	public function index() {

        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 3
        ));
        $this->set('novidades', $produtos);
	}




}
