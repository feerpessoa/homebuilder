<div id="right">
    <h2>Calcular gastos - Quantidade de Tintas</h2>
    <div id="welcome">



        <center><table width='80%' height='20%' border='1'>

                <tr>
                    <td><center><?php echo "Dados da parede" ;?></center></td>
                    <td><center><?php echo "Valores" ;?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Área total da parede" ;?></td>
                    <td><center><?php echo number_format($dimensaoDaParede, 2) . " m²";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Quantidade de Portas" ;?></td>
                    <td><center><?php echo number_format($numeroPortas);?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Dimensões de cada Porta" ;?></td>
                    <td><center><?php echo number_format($larguraPorta, 2) . "m" . " x " . number_format($alturaPorta, 2) . "m";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Quantidade de Janelas" ;?></td>
                    <td><center><?php echo number_format($numeroJanelas);?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Dimensões de cada Janela" ;?></td>
                    <td><center><?php echo number_format($larguraJanela, 2) . "m" . " x " . number_format($alturaJanela, 2) . "m";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Área a ser pintada" ;?></td>
                    <td><center><?php echo number_format($areaParaPintar, 2) . " m²";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Quantidade de Demãos" ;?></td>
                    <td><center><?php echo number_format($quantidadeDemaos);?></center></td>
                </tr>

        </table></center><br /><br />

        <center><table width='80%' height='20%' border='1'>

                <tr>
                    <td><center><?php echo "Tintas" ;?></center></td>
                    <td><center><?php echo "Valores" ;?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Acrílica Econômica" ;?></td>
                    <td><center><?php echo number_format($acrilicaEconomica, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Acrílica Premium Acetinado" ;?></td>
                    <td><center><?php echo number_format($acrilicaPremiumAcetinado, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Acrílica Premium Fosco" ;?></td>
                    <td><center><?php echo number_format($acrilicaPremiumFosco, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Acrílica Standard" ;?></td>
                    <td><center><?php echo number_format($acrilicaStandard, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Esmalte Premium" ;?></td>
                    <td><center><?php echo number_format($esmaltePremium, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Látex Premium" ;?></td>
                    <td><center><?php echo number_format($latexPremium, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Látex Standard" ;?></td>
                    <td><center><?php echo number_format($latexStandard, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Tinta para Gesso" ;?></td>
                    <td><center><?php echo number_format($tintaParaGesso, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Tinta para Piso" ;?></td>
                    <td><center><?php echo number_format($tintaParaPiso, 2) . " litros";?></center></td>
                </tr>

                <tr>
                    <td><?php echo "Verniz Acrílico" ;?></td>
                    <td><center><?php echo number_format($vernizAcrilico, 2) . " litros";?></center></td>
                </tr>

            </table></center>


    </div>
</div>