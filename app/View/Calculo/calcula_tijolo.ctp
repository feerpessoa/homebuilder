<div id="right">
    <h2>Calcular gastos - Tijolos e Blocos</h2>
    <div id="welcome">



        <center><table width='80%' height='20%' border='1'>

           <tr>
               <td><center><?php echo "Tijolos e Blocos" ;?></center></td>
               <td><center><?php echo "Valores" ;?></center></td>
           </tr>

            <tr>
               <td><?php echo "Tijolos comuns de 5cm" ;?></td>
               <td><center><?php echo number_format($tijolosComuns5cm) . " un.";?></center></td>
            </tr>

            <tr>
               <td><?php echo "Tijolos comuns de 10cm" ;?></td>
               <td><center><?php echo number_format($tijolosComuns10cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Tijolos comuns de 20cm" ;?></td>
                <td><center><?php echo number_format($tijolosComuns20cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Tijolos cerâmicos de 10cm" ;?></td>
                <td><center><?php echo number_format($tijolosCeramicos10cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Tijolos cerâmicos de 20cm" ;?></td>
                <td><center><?php echo number_format($tijolosCeramicos20cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Blocos de concreto de 10cm" ;?></td>
                <td><center><?php echo number_format($blocosDeConcreto10cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Blocos de concreto de 15cm" ;?></td>
                <td><center><?php echo number_format($blocosDeConcreto15cm) . " un.";?></center></td>
            </tr>

            <tr>
                <td><?php echo "Blocos de concreto de 20cm" ;?></td>
                <td><center><?php echo number_format($blocosDeConcreto20cm) . " un.";?></center></td>
            </tr>

        </table></center>


    </div>
</div>