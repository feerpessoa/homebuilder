
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('Novo Produto'), array('action' => 'add'), array('class' => '')); ?></li>
        	</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="novidades index">
		
			<h2><?php echo __('Produtos'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						<tr>

															<th><?php echo $this->Paginator->sort('Titulo'); ?></th>
                                                            <th><?php echo $this->Paginator->sort('Preço'); ?></th>
															<th><?php echo $this->Paginator->sort('Preço de leilão'); ?></th>
                                                            <th>Previsualização</th>
															<th class="actions"><?php echo __('Ações'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($novidades as $novidade): ?>
	<tr>

		<td><?php echo h($novidade['Produto']['titulo']); ?>&nbsp;</td>
       <td><?php echo (!empty($novidade['Produto']['preco']))?  'R$' .number_format($novidade['Produto']['preco'], 2, ',', '.') : "Não Informado"; ?>&nbsp;</td>
        <td><?php echo (!empty($novidade['Produto']['preco_leilao']))?  'R$' .number_format($novidade['Produto']['preco_leilao'], 2, ',', '.') : "Não Informado"; ?>&nbsp;</td>

        <td>
            <?php if(isset($novidade['Produto']['dir'])):

                $novidade['Produto']['dir'] = explode("\\",$novidade['Produto']['dir']);
                $novidade['Produto']['dir'] = implode('/',$novidade['Produto']['dir']);

                $img = $novidade['Produto']['dir'].'/'.$novidade['Produto']['filename']?>

                <?php echo $this->Html->image('/'.$img) ?>
            <?php endif; ?>
        </td>
		<td class="actions">
			<?php  echo $this->Html->link(__('Detalhar'), array('action' => 'view', $novidade['Produto']['id']), array('class' => 'btn btn-default btn-xs'));  ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $novidade['Produto']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Apagar'), array('action' => 'delete', $novidade['Produto']['id']), array('class' => 'btn btn-default btn-xs'), __('Tem certeza que deseja apagar este item? %s?', $novidade['Produto']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
 
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Pagina {:page} de {:pages}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('< Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Próximo >') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
