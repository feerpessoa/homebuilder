<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {


	public $components = array('Paginator');

	public function beforeFilter() {
        parent::beforeFilter(); 
        $this->Auth->allow('logout','login','add');
    }
	
	 
	
	public function login() {
	  if(!$this->Auth->loggedIn()){
		if($this->request->is('post')){
	    	if ($this->Auth->login()) {
	        	$this->redirect(array('controller'=>'produtos','action'=>'index'));
	    	} else {
	    		$this->Session->setFlash(__('Usuario ou senha incorretos.'), 'flash/error'); 
	    	}
		}
	  }else{
	  	$this->redirect(array('controller'=>'produtos','action'=>'index'));
	  }
	} 

	public function logout() {
	    $this->redirect($this->Auth->logout());
	}
	
	/*
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}
	 */

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 /*
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('User está inválido.'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
  */

/**
 * add method
 *
 * @return void
 */

	public function add() { 
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('User foi salvo com sucesso!'), 'flash/success');
                $this->redirect(array('controller'=>'produtos','action'=>'index'));
			} else {
				$this->Session->setFlash(__('User não pode ser salvo, por favor tente novamente.'), 'flash/error');
			}
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 /*
	public function edit($id = null) {
        $this->User->id = $id;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('User está inválido.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('User foi salvo com sucesso!'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User não pode ser salvo, por favor tente novamente.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}
  */

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
 /*
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('User está inválido.'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User foi apagado.'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User não pode ser apagado.'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
  * 
  */
}
