<?php $this->TinyMCE->editor(array('theme' => 'advanced', 'mode' => 'textareas')); ?>
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
										<li class="list-group-item"><?php echo $this->Html->link(__('Visualizar Produtos'), array('action' => 'index')); ?></li>
						<li class="list-group-item"><?php echo $this->Html->link(__('Visualizar Imagens'), array('controller' => 'ProdutoImages', 'action' => 'index')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Nova Imagem'), array('controller' => 'ProdutoImages', 'action' => 'add')); ?> </li>
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="novidades form">

            <?php echo $this->Form->create('Produto', array('type'=>'file','inputDefaults' => array('label' => false), 'role' => 'form')); ?>
            <fieldset>
					<h2><?php echo __('Novo Produto'); ?></h2>

                <div class="form-group">
                             <?php echo $this->Form->label('titulo', 'Titulo do produto');?>
                            <?php echo $this->Form->input('titulo', array('class' => 'form-control')); ?>
                </div><!-- .form-group -->

                <div class="form-group">
                    <?php echo $this->Form->label('link', 'Pagina do produto');?>
                        <?php echo $this->Form->input('link', array('class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                       <?php echo $this->Form->label('preco', 'Preço do produto');?>
                       <?php echo $this->Form->input('preco', array('class' => 'form-control')); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->label('preco_leilao', 'Preço de leilão');?>
                    <?php echo $this->Form->input('preco_leilao', array('class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                    <?php echo $this->Form->label('filename', 'Selecionar imagem');?>
                    <?php echo $this->Form->input('filename', array('class' => 'form-control','type' => 'file')); ?>
                </div><!-- .form-group -->

                <div class="form-group">
                    <?php echo $this->Form->input('dir', array('class' => 'form-control','type' => 'hidden')); ?>
                </div><!-- .form-group -->

                <div class="form-group">
                    <?php echo $this->Form->input('filesize', array('class' => 'form-control','type' => 'hidden')); ?>
                </div><!-- .form-group -->

                <div class="form-group">
                    <?php echo $this->Form->input('mimetype', array('class' => 'form-control','type' => 'hidden')); ?>
                </div><!-- .form-group -->



				</fieldset>
			<?php echo $this->Form->submit('Enviar', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			 
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
