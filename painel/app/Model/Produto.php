<?php
App::uses('AppModel', 'Model');
/**
 * Novidade Model
 *
 * @property NovidadeImage $NovidadeImage
 */
class Produto extends AppModel {


    public $displayField = 'nome';

    var $actsAs = array(
        'MeioUpload' => array(
            'filename' => array(
                'thumbsizes' => array(
                    'pequena' => array(
                        'width' => 101,
                        'height' => 100
                    ),
                    'medio' => array(
                        'width' => 190,
                        'height' => 190
                    )
                )
            )
        )
    );

	public $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),


	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */


}
