-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Nov-2014 às 20:27
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `homebuilder_bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `link` varchar(300) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `mimetype` varchar(255) DEFAULT NULL,
  `dir` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `preco_leilao` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `partners`
--

INSERT INTO `partners` (`id`, `nome`, `link`, `filename`, `filesize`, `mimetype`, `dir`, `created`, `modified`, `preco_leilao`) VALUES
(2, 'Casa&Construção', 'http://www.cec.com.br/', 'casadaconstru.png', 3812, 'image/png', 'images\\uploads\\partner', '2014-11-26 17:44:21', '2014-11-26 17:44:21', 10),
(3, 'Baratão da Construção', 'http://grupobaratao.com.br/construcao/', 'logo.png', 8540, 'image/png', 'images\\uploads\\partner', '2014-11-27 19:51:51', '2014-11-27 19:51:51', 11),
(4, 'Telhanorte', 'http://www.telhanorte.com.br/', 'thn_logotipo.png', 7002, 'image/png', 'images\\uploads\\partner', '2014-11-27 19:59:30', '2014-11-27 19:59:30', 12),
(5, 'Nicom', 'http://www.nicom.com.br/', 'logo.gif', 22783, 'image/gif', 'images\\uploads\\partner', '2014-11-27 20:17:21', '2014-11-27 20:17:21', 12.5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(60) NOT NULL,
  `preco` double NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `mimetype` varchar(255) DEFAULT NULL,
  `dir` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `link` varchar(300) NOT NULL,
  `preco_leilao` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `preco`, `filename`, `filesize`, `mimetype`, `dir`, `created`, `modified`, `link`, `preco_leilao`) VALUES
(3, 'BLOCO ESTRUTURAL - 14x19x29 cm', 15, 'bloco_estrutural_g.jpg', 13958, 'image/jpeg', 'images\\uploads\\produto', '2014-11-26 17:46:51', '2014-11-26 17:46:51', 'http://www.terravitta.com.br/produtos_estruturais_estrutural.php', 5),
(4, 'Porcelanato esmaltado bold 47x47cm Wood white Buschinelli', 23.9, '1145673.jpg', 12683, 'image/jpeg', 'images\\uploads\\produto', '2014-11-27 20:00:44', '2014-11-27 20:00:44', 'http://www.telhanorte.com.br/porcelanato-esmaltado-bold-47x47cm-wood-white-buschinelli-1145673/p', 8),
(5, 'Piso cerâmico 50x50cm branco branco Triunfo', 12.29, '1078380.jpg', 22676, 'image/jpeg', 'images\\uploads\\produto', '2014-11-27 20:02:27', '2014-11-27 20:02:27', 'http://www.telhanorte.com.br/piso-ceramico-50x50cm-branco-branco-triunfo-1078380/p', 9),
(6, 'Piso esmaltado 50x50cm modelo 50340 Incefra', 20.5, '1356038.jpg', 43705, 'image/jpeg', 'images\\uploads\\produto', '2014-11-27 20:03:55', '2014-11-27 20:03:55', 'http://www.telhanorte.com.br/piso-esmaltado-50x50cm-modelo-50340-incefra-1356038/p', 13),
(7, 'Telha residencial 244x110cm 5mm Brasilit', 34.9, '49220.jpg', 18053, 'image/jpeg', 'images\\uploads\\produto', '2014-11-27 20:05:15', '2014-11-27 20:05:15', 'http://www.telhanorte.com.br/telha-residencial-244x110cm-5mm-brasilit-49220/p', 12.3),
(8, 'Esmalte sintético Coralit base solvente 900 ml azul del rey', 27.5, '124.jpg', 13933, 'image/jpeg', 'images\\uploads\\produto', '2014-11-27 20:06:24', '2014-11-27 20:06:24', 'http://www.telhanorte.com.br/esmalte-sintetico-coralit-base-solvente-900-ml-azul-del-rey-coral-124/p', 15),
(9, 'Argamassa para Porcelanato Interno / Externo Cinza 20 ', 18.9, '7aa0798a_920f_4602_b316_6993ef4aaccd.png', 53752, 'image/png', 'images\\uploads\\produto', '2014-11-27 20:13:21', '2014-11-27 20:13:21', 'http://www.cec.com.br/material-de-construcao/argamassa/argamassa-para-porcelanato-interno-/-externo-cinza-20-kg?produto=1061202', 18);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(120) NOT NULL,
  `role` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', 'e5091f9902357dd13862984403b6689348264c31', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
