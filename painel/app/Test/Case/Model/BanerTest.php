<?php
App::uses('Baner', 'Model');

/**
 * Baner Test Case
 *
 */
class BanerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.baner'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Baner = ClassRegistry::init('Baner');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Baner);

		parent::tearDown();
	}

}
