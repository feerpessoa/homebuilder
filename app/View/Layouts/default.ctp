<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Home Builder</title>
    <link rel="stylesheet" href="<?=$admLocal?>css/style.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="<?=$admLocal?>slider/jquery.bxslider.css" type="text/css" charset="utf-8" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="<?=$admLocal?>slider/jquery.bxslider.js"></script>

</head>

<script type="text/javascript">
   $(document).ready(function () {
        $('.bxslider').bxSlider({
            autoControls: false,
            mode: 'fade',
            auto:true,
            controls: false
        });
    });
</script>

<body>


<div id="wrapper">
    <div id="header"> </div>
    <div id="left">
        <div id="logo">
            <h1>Home Builder</h1>
            <p>Tudo é possível!</p>
        </div>
        <div id="nav">
            <ul>
                <li class="important"><a href="<?=$admLocal?>Home">Início</a></li>
                <li><a href="<?=$admLocal?>Contato">Contato</a></li>
                <li><a href="<?=$admLocal?>Partners">Parceiros</a></li>
                <li><a href="<?=$admLocal?>Produtos">Produtos</a></li>
                <li><a href="<?=$admLocal?>Calculo">Fazer calculos</a></li>

            </ul>
        </div>

        <div id="news">
            <ul class="bxslider">
          <?php
            foreach ($novidades as $novidade): ?>
                <a href="<?php echo $novidade['Produto']['link']; ?>">

                    <h3>

                        <?php if(isset($novidade['Produto']['dir'])):

                            $novidade['Produto']['dir'] = explode("\\",$novidade['Produto']['dir']);
                            $novidade['Produto']['dir'] = implode('/',$novidade['Produto']['dir']);

                            $img = $novidade['Produto']['dir'].'/thumb/pequena/'.$novidade['Produto']['filename']?>
                           <img src="/Homebuilder/painel/<?=$img?>" />

                        <?php endif; ?>
                    </h3>
                </a>

            <?php endforeach; ?>
                <div class="hr-dots"> </div>
            </ul>
        </div>
    </div>
    <div id="content">

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
    </div>
    <div class="clear"> </div>
    <div id="spacer"> </div>
    <div id="footer">
        <div id="copyright">
            &copy; 2014 Home Builder todos os direitos reservados.
        </div>
        <div id="footerline"></div>
    </div>

</div>
</body>
</html>