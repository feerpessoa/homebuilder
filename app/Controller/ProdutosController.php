<?php
App::uses('AppController', 'Controller');
/**
 * Novidades Controller
 *
 * @property Novidade $Novidade
 * @property PaginatorComponent $Paginator
 */
class ProdutosController extends AppController {


	public function index() {


        $this->paginate = array('limit'=>6, 'order' => array('Produto.created' => 'desc'));

        $noticias = $this->paginate('Produto');

        $this->set(compact('novidades'));

        $this->set('totalinsc', $this->Produto->find('count'));

        $this->set('novidades123', $noticias);


        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 3
        ));
        $this->set('novidades', $produtos);
	}




}
