<div id="right">
    <h2>Calcular gastos - Pisos e Revestimentos</h2>
    <div id="welcome">

        <center><table width='80%' height='20%' border='1'>

            <tr>
                <td><center><?php echo"Dados";?></td></center>
                <td><center><?php echo"Valores"?></td></center>
            </tr>

            <tr>
                <td><?php echo"Área da superfície"?></td>
                <td><center><?php echo number_format($areaSuperficie, 2) . " m²";?></td></center>
            </tr>

            <tr>
                <td><?php echo"Quantidade de Revestimento (reto) "?></td>
                <td><center><?php echo number_format($quantidadeRevestimentoReto, 2) . " m²";?></td></center>
            </tr>
            <tr>
                <td><?php echo"Quantidade de Revestimento (diagonal) "?></td>
                <td><center><?php echo number_format($quantidadeRevestimentoDiagonal, 2). " m²";?></td></center>
            </tr>

        </table></center><br /><br />

        <center><table width='80%' height='20%' border='1'>

                <tr>
                    <td><center><?php echo"Materiais";?></td></center>
        <td><center><?php echo"Valores"?></td></center>
        </tr>

        <tr>
            <td><?php echo"Quantidade de argamassa "?></td>
            <td><center><?php echo number_format($quantidadeArgamassa, 2). " kg";?></td></center>
        </tr>

        <tr>
            <td><?php echo"Quantidade de rejunte Epóxi "?></td>
            <td><center><?php echo number_format($quantidadeRejunteEpoxi, 2). " kg";?></td></center>
        </tr>

        <tr>
            <td><?php echo"Quantidade de rejunte Flexível "?></td>
            <td><center><?php echo number_format($quantidadeRejungeFlexivel, 2). " kg";?></td></center>
        </tr>

        <tr>
            <td><?php echo"Peças necessárias (reto) "?></td>
            <td><center><?php echo number_format($pecasNecessariasReto). " un.";?></td></center>
        </tr>

        <tr>
            <td><?php echo"Peças necessárias (diagonal) "?></td>
            <td><center><?php echo number_format($pecasNecessariasDiagonal). " un.";?></td></center>
        </tr>

        </table></center>
    </div>
</div>