<div id="right">
    <h2>Produtos mais vendidos</h2>
    <div id="welcome">
        <div class="parceiros">
            <ul>

                <?php
                foreach ($novidades123 as $novidades123): ?>

                    <li><a href="<?php echo $novidades123['Produto']['link']; ?>"> <p><?php echo h($novidades123['Produto']['titulo']); ?></p>
                            <?php if(isset($novidades123['Produto']['dir'])):

                                $novidades123['Produto']['dir'] = explode("\\",$novidades123['Produto']['dir']);
                                $novidades123['Produto']['dir'] = implode('/',$novidades123['Produto']['dir']);

                                $img = $novidades123['Produto']['dir'].'/thumb/pequena/'.$novidades123['Produto']['filename']?>
                                <img src="/Homebuilder/painel/<?=$img?>" alt="" />
                            <?php endif; ?>
                            <p>
                                R$  <?php echo 'R$'.(!empty($novidades123['Produto']['preco']))?  'R$' .number_format($novidades123['Produto']['preco'], 2, ',', '.') : "Não Informado"; ?>
                            </p>
                        </a></li>

                    <div class="hr-dots"> </div>
                <?php endforeach; ?>

            </ul>


        </div>
        <div class="controles">
            <?=$this->Paginator->prev('<< Anterior', array(), NULL, array('class' => 'disabled'));?>

            <?=$this->Paginator->numbers();?>

            <?=$this->Paginator->next('Próximo >>', array(), NULL, array('class' => 'disabled'));?>

        </div>
    </div>
</div>