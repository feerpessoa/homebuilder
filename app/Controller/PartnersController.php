<?php
App::uses('AppController', 'Controller');
/**
 * Novidades Controller
 *
 * @property Novidade $Novidade
 * @property PaginatorComponent $Paginator
 */
class PartnersController extends AppController {

    public function index() {
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);


        $this->paginate = array('limit'=>10, 'order' => array('Partner.created' => 'desc'));

        $noticias = $this->paginate('Partner');

        $this->set(compact('parceiro'));

        $this->set('totalinsc', $this->Partner->find('count'));

        $this->set('parceiro', $noticias);


    }

}
