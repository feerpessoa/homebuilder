<?php


 
App::uses('Controller', 'Controller');



class AppController extends Controller {


    public $components = array('Paginator','Session');

    public $admLocal = "/Homebuilder/";

    public $uses = array('Produto','Partner');

    public function beforeFilter(){

        $this->set('admLocal',$this->admLocal);

    }

}


?>