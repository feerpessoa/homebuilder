<?php
App::uses('AppController', 'Controller');

 ini_set('default_charset','UTF-8');


class CalculoController extends AppController {


	public function index() {
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);
	}

    public function pisos() {
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);
    }

    public function pisos_calcula_tela() {
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);

            if(
                !empty($this->request->data['num0']) AND
                !empty($this->request->data['num1']) AND
                !empty($this->request->data['num2']) AND
                !empty($this->request->data['num3'])
            )
            {
                $areaSuperficie = ($this->request->data['num2'] * $this->request->data['num3']);

                $quantidadeRevestimentoReto  = ( $this->request->data['num0'] * $this->request->data['num1'] + ($this->request->data['num0'] * $this->request->data['num1'])*10/100);
                $quantidadeRevestimentoDiagonal  = ( $this->request->data['num0'] * $this->request->data['num1'] + ($this->request->data['num0'] * $this->request->data['num1'])*15/100);

                $quantidadeArgamassa = ($areaSuperficie * 4);

                $quantidadeRejunteEpoxi = ($areaSuperficie * 3.16);

                $quantidadeRejungeFlexivel = ($areaSuperficie * 3.5);

                $pecasNecessariasReto = ($quantidadeRevestimentoReto / $areaSuperficie)*100;
                $pecasNecessariasDiagonal = ($quantidadeRevestimentoDiagonal / $areaSuperficie)*100;


                if(isset($this->request->data['visualizar'])) // se for pra visualizar seta as variaveis
                {
                    $this->set("quantidadeRevestimentoReto",$quantidadeRevestimentoReto);
                    $this->set("quantidadeRevestimentoDiagonal",$quantidadeRevestimentoDiagonal);
                    $this->set("areaSuperficie",$areaSuperficie);
                    $this->set("quantidadeArgamassa",$quantidadeArgamassa);
                    $this->set("quantidadeRejunteEpoxi",$quantidadeRejunteEpoxi);
                    $this->set("quantidadeRejungeFlexivel",$quantidadeRejungeFlexivel);
                    $this->set("pecasNecessariasReto",$pecasNecessariasReto);
                    $this->set("pecasNecessariasDiagonal",$pecasNecessariasDiagonal);
                }

                else if(isset($this->request->data["excel"])){
                    $html = "
                        <table width='80%' height='20%' border='1'>

                            <tr>
                                <td> <strong> <center> Dados </center></strong> </td>
                                <td> <strong> <center> Valores </center></strong> </td>
                            </tr>

                            <tr>
                                <td> Area da superficie </td>
                                <td> $areaSuperficie m2 </td>
                            </tr>

                            <tr>
                            <td> Quantidade de Revestimento (reto) </td>
                            <td> $quantidadeRevestimentoReto m2 </td>
                            </tr>

                            <tr>
                                <td>Quantidade de Revestimento (diagonal) </td>
                                <td> $quantidadeRevestimentoDiagonal m2</td>
                            </tr>

                <tr> </tr>

                            <tr>
                                <td> <strong> <center> Materiais </center> </strong> </td>
                                <td> <strong> <center> Valores </center> </strong> </td>
                            </tr>

                             <tr>
                                <td> Quantidade de argamassa </td>
                                <td> $quantidadeArgamassa kg</td>
                             </tr>

                             <tr>
                                <td> Quantidade de rejunte Epoxi </td>
                                <td> $quantidadeRejunteEpoxi kg</td>
                             </tr>

                             <tr>
                                <td> Quantidade de rejunte Flexivel </td>
                                <td> $quantidadeRejungeFlexivel kg</td>
                             </tr>

                             <tr>
                                <td> Pecas necessarias (reto) </td>
                                <td> $pecasNecessariasReto unidades</td>
                             </tr>

                             <tr>
                                <td> Pecas necessarias (diagonal) </td>
                                <td> $pecasNecessariasDiagonal unidades</td>
                             </tr>

                        </table>
                     ";

                    header("Content-type: application/vnd.ms-excel");
                    header("Content-type: application/force-download");
                    header("Content-Disposition: attachment; filename=ResultadoCalculoPisos.xls");
                    header("Pragma: no-cache");

                    echo $html;
                    exit();
                }

            }else{

                pr("Digite todos os campos!");
                exit();

            }
    }

    public function telhas(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);
    }

    public function calcula_telha(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);

        if(
            !empty($this->request->data['num0']) AND
            !empty($this->request->data['num1'])
        )

        {
            $larguraCobertura = $this->request->data['num0'];
            $alturaCobertura = $this->request->data['num1'];
            $numeroTelhaFrancesa   = ($larguraCobertura * $alturaCobertura * 1.6);
            $numeroTelhaPaulista   = ($larguraCobertura * $alturaCobertura * 2.5);
            $numeroTelhaPlan       = ($larguraCobertura * $alturaCobertura * 2.6);


            if(isset($this->request->data['visualizar'])) // se for pra visualizar seta as variaveis
            {
                $this->set("larguraCobertura",$larguraCobertura);
                $this->set("alturaCobertura",$alturaCobertura);
                $this->set("numeroTelhaFrancesa",$numeroTelhaFrancesa);
                $this->set("numeroTelhaPaulista",$numeroTelhaPaulista);
                $this->set("numeroTelhaPlan",$numeroTelhaPlan);
            }

            else if(isset($this->request->data["excel"])){
                $html = "
                        <table width='80%' height='20%' border='1'>

                            <tr>
                                <td> <strong> <center> Telhas </center> </strong> </td>
                                <td> <strong> <center> Quantidade </center> </strong> </td>
                            </tr>

                             <tr>
                                <td> Numero de telhas Francesa </td>
                                <td> $numeroTelhaFrancesa unidades</td>
                             </tr>

                             <tr>
                                <td> Numero de telhas Paulista </td>
                                <td> $numeroTelhaPaulista unidades</td>
                             </tr>

                             <tr>
                                <td> Numero de telhas Plan </td>
                                <td> $numeroTelhaPlan unidades</td>
                             </tr>

                        </table>
                     ";

                header("Content-type: application/vnd.ms-excel");
                header("Content-type: application/force-download");
                header("Content-Disposition: attachment; filename=ResultadoCalculoTelhas.xls");
                header("Pragma: no-cache");

                echo $html;
                exit();
            }

        } else{

            pr("Digite todos os campos!");
            exit();

        }
    }

    public function tijolos(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);
    }

    public function calcula_tijolo(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);

        if(
            !empty($this->request->data['num0']) AND
            !empty($this->request->data['num1'])
        )
        {
            $areaSuperficie  = ( $this->request->data['num0'] * $this->request->data['num1']);

            $tijolosComuns5cm =  ($areaSuperficie * 46);
            $tijolosComuns10cm = ($areaSuperficie * 84);
            $tijolosComuns20cm = ($areaSuperficie * 149);

            $tijolosCeramicos10cm = ($areaSuperficie * 25);
            $tijolosCeramicos20cm = ($areaSuperficie * 47);

            $blocosDeConcreto10cm = ($areaSuperficie * 14);
            $blocosDeConcreto15cm = ($areaSuperficie * 14);
            $blocosDeConcreto20cm = ($areaSuperficie * 14);


            if(isset($this->request->data['visualizar'])) // se for pra visualizar seta as variaveis
            {
                $this->set("tijolosComuns5cm",$tijolosComuns5cm);
                $this->set("tijolosComuns10cm",$tijolosComuns10cm);
                $this->set("tijolosComuns20cm",$tijolosComuns20cm);
                $this->set("tijolosCeramicos10cm",$tijolosCeramicos10cm);
                $this->set("tijolosCeramicos20cm",$tijolosCeramicos20cm);
                $this->set("blocosDeConcreto10cm",$blocosDeConcreto10cm);
                $this->set("blocosDeConcreto15cm",$blocosDeConcreto15cm);
                $this->set("blocosDeConcreto20cm",$blocosDeConcreto20cm);
            }

              else if(isset($this->request->data["excel"])){ // se for para o excel baixa o arquivo
                $html = "
                        <table width='80%' height='20%' border='1'>

                            <tr>
                                <td> <strong> <center> Tijolos e Blocos </center> </strong> </td>
                                <td> <strong> <center> Quantidade </center> </strong> </td>
                            </tr>

                             <tr>
                                <td> Tijolos comuns de 5cm </td>
                                <td> $tijolosComuns5cm unidades</td>
                             </tr>

                             <tr>
                                <td> Tijolos comuns de 10cm </td>
                                <td> $tijolosComuns10cm unidades</td>
                             </tr>

                             <tr>
                                <td> Tijolos comuns de 20cm </td>
                                <td> $tijolosComuns20cm unidades</td>
                             </tr>

                             <tr>
                                <td> Tijolos ceramicos de 10cm </td>
                                <td> $tijolosCeramicos10cm unidades</td>
                             </tr>

                             <tr>
                                <td> Tijolos ceramicos de 20cm </td>
                                <td> $tijolosCeramicos20cm unidades</td>
                             </tr>

                             <tr>
                                <td> Blocos de concreto de 10cm </td>
                                <td> $blocosDeConcreto10cm unidades</td>
                             </tr>

                             <tr>
                                <td> Blocos de concreto de 15cm </td>
                                <td> $blocosDeConcreto15cm unidades</td>
                             </tr>

                             <tr>
                                <td> Blocos de concreto de 20cm </td>
                                <td> $blocosDeConcreto20cm unidades</td>
                             </tr>

                        </table>
                     ";

                  header("Content-type: application/vnd.ms-excel");
                  header("Content-type: application/force-download");
                  header("Content-Disposition: attachment; filename=ResultadoCalculoTijolos.xls");
                  header("Pragma: no-cache");

                echo $html;
                exit();
            }

        } else{

            pr("Digite todos os campos!");
            exit();

        }
    }

    public function tintas(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);
    }

    public function calcula_tinta(){
        $produtos = $this->Produto->find('all',array(
            'order'=>array('Produto.preco_leilao'=>'desc'),
            'limit' => 2
        ));
        $this->set('novidades', $produtos);

        if(
            !empty($this->request->data['num0']) AND
            !empty($this->request->data['num1']) AND
            !empty($this->request->data['num2']) AND
            !empty($this->request->data['num3']) AND
            !empty($this->request->data['num4']) AND
            !empty($this->request->data['num5']) AND
            !empty($this->request->data['num6']) AND
            !empty($this->request->data['num7']) AND
            !empty($this->request->data['num8'])
        )

        {
            $dimensaoDaParede = $this->request->data['num0']*$this->request->data['num1'];
            $numeroJanelas = $this->request->data['num2'];
            $larguraJanela = $this->request->data['num3'];
            $alturaJanela = $this->request->data['num4'];
            $numeroPortas = $this->request->data['num5'];
            $larguraPorta = $this->request->data['num6'];
            $alturaPorta = $this->request->data['num7'];
            $quantidadeDemaos = $this->request->data['num8'];

            $areaDeJanelas = $numeroJanelas * ($larguraJanela * $alturaJanela);
            $areaDePortas = $numeroPortas * ($larguraPorta * $alturaPorta);
            $areaParaPintar = $dimensaoDaParede - $areaDeJanelas - $areaDePortas;

            $acrilicaEconomica = $quantidadeDemaos * ($areaParaPintar / 8.30);
            $acrilicaPremiumAcetinado = $quantidadeDemaos * ($areaParaPintar / 10);
            $acrilicaPremiumFosco = $quantidadeDemaos * ($areaParaPintar / 21);
            $acrilicaStandard = $quantidadeDemaos * ($areaParaPintar / 13.80);
            $esmaltePremium = $quantidadeDemaos * ($areaParaPintar / 11);
            $latexPremium = $quantidadeDemaos * ($areaParaPintar / 16.60);
            $latexStandard = $quantidadeDemaos * ($areaParaPintar / 12.5);
            $tintaParaGesso = $quantidadeDemaos * ($areaParaPintar / 6.5);
            $tintaParaPiso = $quantidadeDemaos * ($areaParaPintar / 5.5);
            $vernizAcrilico = $quantidadeDemaos * ($areaParaPintar / 12.5);


            if(isset($this->request->data['visualizar'])) // se for pra visualizar seta as variaveis
            {
                $this->set("dimensaoDaParede",$dimensaoDaParede);
                $this->set("numeroJanelas", $numeroJanelas);
                $this->set("larguraJanela",$larguraJanela);
                $this->set("alturaJanela",$alturaJanela);
                $this->set("numeroPortas",$numeroPortas);
                $this->set("larguraPorta",$larguraPorta);
                $this->set("alturaPorta",$alturaPorta);
                $this->set("areaDeJanelas",$areaDeJanelas);
                $this->set("areaDePortas",$areaDePortas);
                $this->set("areaParaPintar", $areaParaPintar);

                $this->set("quantidadeDemaos", $quantidadeDemaos);

                $this->set("acrilicaEconomica", $acrilicaEconomica);
                $this->set("acrilicaPremiumAcetinado", $acrilicaPremiumAcetinado);
                $this->set("acrilicaPremiumFosco", $acrilicaPremiumFosco);
                $this->set("acrilicaStandard", $acrilicaStandard);
                $this->set("esmaltePremium", $esmaltePremium);
                $this->set("latexPremium", $latexPremium);
                $this->set("latexStandard", $latexStandard);
                $this->set("tintaParaGesso", $tintaParaGesso);
                $this->set("tintaParaPiso", $tintaParaPiso);
                $this->set("vernizAcrilico", $vernizAcrilico);
            }

            else if(isset($this->request->data["excel"])){
                $html = "
                        <table width='80%' height='20%' border='1'>

                            <tr>
                                <td> <strong> <center> Dados </center></strong> </td>
                                <td> <strong> <center> Valores </center></strong> </td>
                            </tr>

                            <tr>
                                <td> Area total da parede </td>
                                <td> $dimensaoDaParede m2 </td>
                            </tr>

                            <tr>
                                <td> Quantidade de Portas </td>
                                <td> $numeroPortas unidades </td>
                            </tr>

                            <tr>
                                <td> Dimensoes de cada Porta </td>
                                <td> $larguraPorta m x $alturaPorta m</td>
                            </tr>

                            <tr>
                                <td> Quantidade de Janelas </td>
                                <td> $numeroJanelas unidades </td>
                            </tr>

                            <tr>
                                <td> Dimensoes de cada Janela </td>
                                <td> $larguraJanela m x $alturaJanela m </td>
                            </tr>

                            <tr>
                                <td> Area a ser pintada </td>
                                <td> $areaParaPintar m2 </td>
                            </tr>

                            <tr>
                                <td> Quantidade de Demaos </td>
                                <td> $quantidadeDemaos unidades </td>
                            </tr>

                <tr> </tr>

                             <tr>
                                <td> <strong> <center> Tintas </center></strong> </td>
                                <td> <strong> <center> Valores </center></strong> </td>
                            </tr>

                            <tr>
                                <td> Acrilica Economica </td>
                                <td> $acrilicaEconomica litros </td>
                            </tr>

                            <tr>
                                <td> Acrilica Premium Acetinado </td>
                                <td> $acrilicaPremiumAcetinado litros </td>
                            </tr>

                            <tr>
                                <td> Acrilica Premium Fosco </td>
                                <td> $acrilicaPremiumFosco litros </td>
                            </tr>

                            <tr>
                                <td> Acrilica Standard </td>
                                <td> $acrilicaStandard litros </td>
                            </tr>

                            <tr>
                                <td> Esmalte Premium </td>
                                <td> $esmaltePremium litros </td>
                            </tr>

                            <tr>
                                <td> Latex Premium </td>
                                <td> $latexPremium litros </td>
                            </tr>

                            <tr>
                                <td> Latex Standard </td>
                                <td> $latexStandard litros </td>
                            </tr>

                            <tr>
                                <td> Tinta para Gesso </td>
                                <td> $tintaParaGesso litros </td>
                            </tr>

                            <tr>
                                <td> Tinta para Piso </td>
                                <td> $tintaParaPiso litros </td>
                            </tr>

                            <tr>
                                <td> Verniz Acrilico </td>
                                <td> $vernizAcrilico litros </td>
                            </tr>

                          </table>
                     ";

                header("Content-type: application/vnd.ms-excel");
                header("Content-type: application/force-download");
                header("Content-Disposition: attachment; filename=ResultadoCalculoTintas.xls");
                header("Pragma: no-cache");

                echo $html;
                exit();
            }


        } else{

            pr("Digite todos os campos!");
            exit();
        }
    }
}