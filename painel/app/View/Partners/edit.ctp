
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
										<li class="list-group-item"><?php echo $this->Form->postLink(__('Apagar'), array('action' => 'delete', $this->Form->value('Partner.id')), null, __('Tem certeza que deseja apagar este item? %s', $this->Form->value('Partner.id'))); ?></li>
										<li class="list-group-item"><?php echo $this->Html->link(__('Visualizar Parceiros'), array('action' => 'index')); ?></li>
						<li class="list-group-item"><?php echo $this->Html->link(__('Visualizar Produtos'), array('controller' => 'Produtos', 'action' => 'index')); ?> </li>
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="novidades form">

            <?php echo $this->Form->create('Partner', array('type'=>'file','inputDefaults' => array('label' => false), 'role' => 'form')); ?>
            <fieldset>
					<h2><?php echo __('Atualizar Parceiro'); ?></h2>
			<div class="form-group">
		<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

                <?php echo $this->Form->label('nome', 'Titulo');?>
                <?php echo $this->Form->input('nome', array('class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
            <?php echo $this->Form->label('link', 'Link do parceiro');?>
            <?php echo $this->Form->input('link', array('class' => 'form-control')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
            <?php echo $this->Form->label('preco_leilao', 'Preço de leilão');?>
            <?php echo $this->Form->input('preco_leilao', array('class' => 'form-control')); ?>
        </div>

        <div class="form-group">
            <?php echo $this->Form->label('filename', 'Selecionar Imagem');?>
            <?php echo $this->Form->input('filename', array('class' => 'form-control','type' => 'file')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
            <?php echo $this->Form->input('dir', array('class' => 'form-control','type' => 'hidden')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
            <?php echo $this->Form->input('filesize', array('class' => 'form-control','type' => 'hidden')); ?>
        </div><!-- .form-group -->

        <div class="form-group">
            <?php echo $this->Form->input('mimetype', array('class' => 'form-control','type' => 'hidden')); ?>
        </div><!-- .form-group -->

				</fieldset>
			<?php echo $this->Form->submit('Enviar', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			 
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
